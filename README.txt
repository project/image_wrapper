CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Installation
 * Configuration

INTRODUCTION
------------

Add wrapper to image which are uploaded through CKEditor, IMCE.

INSTALLTION
-----------

1) Copy image_wrapper directory to your modules directory.

2) Enable the module at module configuration page.

CONFIGURATION
-------------
 * Configure the Style for Image Wrapper
 
 - Use the administration configuration page and help (Image Wrapper)
 
 - Configuration URL
   admin/config/development/image-wrapper page.
 
 - Alter the node body content before rendering it in tpl. 
   
   $variables['wrapper_body_content'] = $html_body_content;
   
 - Check whether its empty or not then Print '$wrapper_body_content' variable to
   'node.tpl.php' or 'node--[CONTENT_TYPE].tpl.php' file.
   
   Example : 
   
   if (!empty($wrapper_body_content)) {
     print render($wrapper_body_content);
   }
   else {
     print render($content);
   }


-- FEATURES --

- Add wrapper to images.
- Add wrapper to image which are uploaded through CKEditor, IMCE.
 